#include "Food.h"

Food::Food() : Product::Product(){}
Food::Food(const string &name, bool eatable, bool fresh, int price) : Product( name, eatable), fresh(fresh),
                                                                      price(price) {}

bool Food::isFresh() const {
    return fresh;
}
void Food::setFresh(bool fresh) {
    Food::fresh = fresh;
}
int Food::getPrice() const {
    return price;
}
void Food::setPrice(int price) {
    Food::price = price;
}

void Food::setAll() {
    Product::setAll();
    cout    << "Is this product fresh?(1/0)\n";
    cin     >> eatable;
    cout    << "Enter price of this product:\n";
    cin     >> name;
}

ostream &Food::show(ostream &output) {
    Product::show(output);
    output   << "it costs "
             << price;
    if (fresh) {
        output << " and it's fresh!\n";
    }
    else{
        output << " and it's not fresh!\n";
    }
    return output;
}
istream &Food::read(istream &input) {
    Product::read(input);
    input >> fresh
          >> price;
    return input;
}

//Food Food::operator=(Food food) {
//    this->name = food.name;
//    this->eatable = food.eatable;
//    this->fresh = food.fresh;
//    this->price = food.price;
//    return *this;
//}