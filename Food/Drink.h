#ifndef VASIL_DRINKS_H
#define VASIL_DRINKS_H

#include <iostream>
#include "Product.h"

using namespace std;

class Drink : public Product{
private:
    string colour;
    int degree;
    ostream &show(ostream &output) override;
    istream &read(istream &input) override;
public:
    Drink();
    Drink(const string &name, bool eatable, string colour, int degree);

    string getcolour() const;
    void setcolour(string colour);
    int getDegree() const;
    void setDegree(int degree);
    void setAll();
};


#endif //VASIL_DRINKS_H
