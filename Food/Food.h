#ifndef VASIL_FOOD_H
#define VASIL_FOOD_H

#include <iostream>
#include "Product.h"

using namespace std;

class Food : public Product {
private:
    bool fresh;
    int price;
    ostream &show(ostream &output) override;
    istream &read(istream &input) override;
public:
    Food();
    Food(const string &name, bool eatable, bool fresh, int price);

    bool isFresh() const;
    void setFresh(bool fresh);
    int getPrice() const;
    void setPrice(int price);
    void setAll();

//    Food operator=(Food food);
};


#endif //VASIL_FOOD_H
