#ifndef VASIL_PRODUCT_H
#define VASIL_PRODUCT_H

#include <iostream>

using namespace std;

class Product {
protected:
    string name;
    bool eatable;
    virtual ostream& show(ostream& output);
    virtual istream& read(istream& input);
public:
    Product();
    Product(const string &name, bool eatable);
    friend ostream& operator<<(ostream& output, Product& product);
    friend istream& operator>>(istream& input, Product& product);

    bool isEatable() const;
    void setEatable(bool eatable);
    const string &getName() const;
    void setName(const string &name);
    void setType(char type);

    void setAll();
};


#endif //VASIL_PRODUCT_H
