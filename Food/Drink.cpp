#include "Drink.h"

Drink::Drink() : Product::Product(){}
Drink::Drink(const string &name, bool eatable, string colour, int degree) : Product(name, eatable), colour(colour),
                                                                            degree(degree) {}

string Drink::getcolour() const {
    return colour;
}
void Drink::setcolour(string colour) {
    Drink::colour = colour;
}
int Drink::getDegree() const {
    return degree;
}
void Drink::setDegree(int degree) {
    Drink::degree = degree;
}

void Drink::setAll() {
    Product::setAll();
    cout    << "Enter colour of this product:\n";
    cin     >> eatable;
    cout    << "Enter amount of degrees of this product:\n";
    cin     >> name;
}

ostream &Drink::show(ostream &output) {
    Product::show(output);
    output << "colour is "
           << colour
           << ", amount of degrees is "
           << degree
           << '.'
           << endl;
    return output;
}
istream &Drink::read(istream &input) {
    Product::read(input);
    input >> colour
          >> degree;
    return input;
}