#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <climits>
#include "Product.h"
#include "Food.h"
#include "Drink.h"

using namespace std;

int global_size;

Product** readFromFile(){
    int size;
    fstream file("../fileToRead.txt");
    if (!file.is_open()) {
        throw 1;
    }
    file >> size;
    global_size = size;
    auto ** array = new Product*[size];
    bool isDrink;
    for(int i = 0; i < size; i++){
        file >> isDrink;
        if(isDrink){
            array[i] = new Drink;
            if (!(file >> *array[i])){
                throw 'c';
            };
        }
        else{
            array[i] = new Food();
            if (!(file >> *array[i])){
                throw 'c';
            };
        }
    }
    return  array;
}

vector<Drink*> findDrinks(Product **product, int size) {
    vector<Drink*> array;
    Drink* temp;
    for (int i = 0; i < size; i++) {
        temp = dynamic_cast<Drink*>(product[i]);
        if (temp != nullptr) {
            array.push_back(temp);
        }
    }
    return array;
}
vector<Food*> findFood(Product **product, int size) {
    vector<Food*> array;
    Food* temp;
    for (int i = 0; i < size; i++) {
        temp = dynamic_cast<Food*>(product[i]);
        if (temp != nullptr) {
            array.push_back(temp);
        }
    }
    return array;
}

void findMaxOrMinBySelectedField(Product **&array, const int &size, const int field, const bool &maximum) {
    int index(0);
    switch (field) {
        case 1: {
            if (maximum) {
                string max = array[0]->getName();
                for (int i = 1; i < size; i++) {
                    if (array[i]->getName() > max) {
                        max = array[i]->getName();
                        index = i;
                    }
                }
                cout << "Info about max name.\n";
            }
            else {
                string min = array[0]->getName();
                for (int i = 1; i < size; i++) {
                    if (array[i]->getName() < min) {
                        min = array[i]->getName();
                        index = i;
                    }
                }
                cout << "Info about min name.\n";
            }
            cout << *array[index];
            return;
        }
        case 2: {
            if (maximum) {
                char type('a');
                int max(0);
                Drink* tempDrink;
                Food * tempFood;
                for (int i = 1; i < size; i++) {
                    tempDrink = dynamic_cast<Drink*>(array[i]);
                    if (tempDrink != nullptr) {
                        if (tempDrink->getDegree() > max) {
                            max = tempDrink->getDegree();
                            index = i;
                            type = 'd';
                        }
                    }
                    else {
                        tempFood = dynamic_cast<Food*>(array[i]);
                        if (tempFood->getPrice() > max) {
                            max = tempFood->getPrice();
                            index = i;
                            type = 'f';
                        }
                    }
                }
                if (type == 'd') {
                    cout << "Info about max degree.\n";
                }
                else {
                    cout << "Info about max price.\n";
                }
            } else {
                char type('a');
                int min(INT_MAX);
                Drink* tempDrink;
                Food * tempFood;
                for (int i = 1; i < size; i++) {
                    tempDrink = dynamic_cast<Drink*>(array[i]);
                    if (tempDrink != nullptr) {
                        if (tempDrink->getDegree() < min) {
                            min = tempDrink->getDegree();
                            index = i;
                            type = 'd';
                        }
                    }
                    else {
                        tempFood = dynamic_cast<Food*>(array[i]);
                        if (tempFood->getPrice() < min) {
                            min = tempFood->getPrice();
                            index = i;
                            type = 'f';
                        }
                    }
                }
                if (type == 'd') {
                    cout << "Info about min degree.\n";
                }
                else {
                    cout << "Info about min price.\n";
                }
            }
            cout << *array[index];
            return;
        }
        default: {
            return;
        }
    }
}
void sortByFirstField(Product **&array, const int &size) {
    Product *temp;
    for (int i = 0; i < size - 1; i++) {
        for (int j = 0; j < size - 1; j++) {
            if (array[j]->getName() > array[j + 1]->getName()) {
                temp = array[j];
                array[j] = array[j + 1];
                array[j + 1] = temp;
            }
        }
    }
}
void findPriceBetween(vector<Food*> array, int less, int more) {
    for_each(array.begin(), array.end(),[less, more](Food* food) {
        if (food->getPrice() < less && food->getPrice() > more) {
            cout << *food;
        }
    });
}

void outputAll(Product **&array, const int &size) {
    for(int i = 0; i < size; i++){
        cout << *array[i];
    }
}

int menu() {
    int choose(0);
    do {
        cout << "\t\t____________MENU____________\n"
             << "\t\t\t Choose one:\n"
             << "1 - sort by first field;\n"
             << "2 - find max/min element of array by selected field;\n"
             << "3 - divide array into two separate types;\n"
             << "4 - find and write to file by selected condition.\n";
        cin >> choose;
    } while (choose > 4 || choose < 1);
    return choose;
}

int main() {
    bool oneMore(true);
    int choose(0);
    Product **array;
    try {
        array = readFromFile();
    }
    catch (int) {
        cout << "Wrong path!\n";
        return 0;
    }
    catch (char) {
        cout << "Wrong info in file!\n";
        return 0;
    }
    do {
        choose = menu();
        switch (choose) {
            case 1: {
                sortByFirstField(array, global_size);
                outputAll(array, global_size);
                break;
            }
            case 2 : {
                int field(0);
                bool max(false);
                do {
                    cout << "Do you want to find max?(0/1)";
                    cin >> max;
                    if (max) {
                        cout << "Choose fiend to find max:\n";
                    } else {
                        cout << "Choose fiend to find min:\n";
                    }
                    cout << "1 - name;\n"
                         << "2 - price/degree.\n";
                    cin >> field;
                } while (field > 3 || field < 1);
                findMaxOrMinBySelectedField(array, global_size, field, max);
                break;
            }
            case 3: {
                vector<Food*>foodArray = findFood(array, global_size);
                vector<Drink*>drinkArray = findDrinks(array, global_size);
                cout << "\t\tFood array:\n";
                for_each(foodArray.begin(), foodArray.end(), [](Food* food){
                    cout << *food;
                });
                cout << "\t\tDrinks array:\n";
                for_each(drinkArray.begin(), drinkArray.end(), [](Drink* drink){
                    cout << *drink ;
                });
                break;
            }
            case 4: {
                int less(0);
                int more(0);
                cout << "Enter interval of price.\nEnter more than: ";
                cin  >> more;
                cout << "Enter less than: ";
                cin  >> less;
                cout << "\t\tCondition is price more than "
                     << more
                     << " and less than "
                     << less
                     << ".\n";
                vector<Food*> vectorFood = findFood(array, global_size);
                findPriceBetween(vectorFood, less, more);
                break;
            }
            default: {
                return 0;
            }
        }
        cout << "Another one?(0/1)\n";
        cin  >> oneMore;
    } while (oneMore);
    return 0;
}