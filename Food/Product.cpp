#include "Product.h"

Product::Product() : eatable(0), name("NO NAME") {}
Product::Product( const string &name, bool eatable) : eatable(eatable), name(name) {}

bool Product::isEatable() const {
    return eatable;
}
void Product::setEatable(bool eatable) {
    Product::eatable = eatable;
}
const string &Product::getName() const {
    return name;
}
void Product::setName(const string &name) {
    Product::name = name;
}

void Product::setAll() {
    cout    << "Enter name of this product:\n";
    cin     >> name;
    cout    << "Is this product eatable?(1/0)\n";
    cin     >> eatable;
}

ostream &operator<<(ostream &output, Product &product) {
    return product.show(output);
}
istream &operator>>(istream &input, Product& product) {
    return product.read(input);
}

istream &Product::read(istream &input) {
    input >> name
          >> eatable;
    return input;
}
ostream& Product::show(ostream& output) {
    output << name
           << " is ";
    if (eatable) {
        output << "eatable, ";
    }
    else{
        output << "uneatable, ";
    }
    return output;
}