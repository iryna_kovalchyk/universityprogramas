#include <iostream>

#include "Transport2.h"

int global_size;

Transport * enter() {
    int size;
    cout << "enter num of array:";
    cin >> size;
    global_size = size;
    Transport *transports = new Transport[size];
    cout << "enter name,kind of energy using and num of human inside:";
    string name;
    string kind_of_energy;
    int num_of_human_inside;
    for (int i = 0; i < size; i++) {
        cin >> name
            >> kind_of_energy
            >> num_of_human_inside;
        transports[i].setName(name);
        transports[i].setKind_of_energy(kind_of_energy);
        transports[i].setNum_of_human(num_of_human_inside);

    }
    return transports;
}


int main() {
    Transport * transport = enter();
    for (int i = 0; i < global_size; i++) {
        if (transport[i].getNum_of_human() > 5) {
            transport[i].print();
        }
    }

    return 0;

}