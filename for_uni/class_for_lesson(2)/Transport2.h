

#ifndef CLASS_FOR_LESSON_2_TRANSPORT2_H
#define CLASS_FOR_LESSON_2_TRANSPORT2_H

#include <iostream>

using namespace std;

class Transport{
private:
    string name;
    string kind_of_energy;
    int num_of_human_inside;
public:
    Transport(string & name, string & kind_of_energy, int num_of_human_inside);
    Transport();

    void print();
    void setNum_of_human(int num_of_human_inside);
    void setName(string name);
    void setKind_of_energy(string kind_of_energy);
    int getNum_of_human() const ;

};

#endif //CLASS_FOR_LESSON_2_TRANSPORT2_H
