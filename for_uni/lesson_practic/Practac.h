#ifndef LESSON_PRACTIC_PRACTAC_H
#define LESSON_PRACTIC_PRACTAC_H

using  namespace std;

class Vector{
private:
    int count;
    int * array;
public:
    Vector();
    Vector(int c);
    //Vector(int count, int*array);
    Vector(Vector& Copy);
    ~Vector();
    void Input();
    void Show();
    int Max();
    int Scalar(Vector & array2);
};

#endif //LESSON_PRACTIC_PRACTAC_H
