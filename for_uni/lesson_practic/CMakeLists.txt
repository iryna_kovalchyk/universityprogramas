cmake_minimum_required(VERSION 3.8)
project(lesson_practic)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES main.cpp Practac.h vec.cpp)
add_executable(lesson_practic ${SOURCE_FILES})