#ifndef HOUSE_ADMIN_H
#define HOUSE_ADMIN_H

#include "House.h"

class Admin: public House {
private:

    int amount_of_office;
    int amount_of_terminal;
public:
    Admin();
   void House_stily() override;

    Admin(int amount_of_floore, int amount_of_exit, int amount_of_office, int amount_of_terminal);
};


#endif //HOUSE_ADMIN_H
