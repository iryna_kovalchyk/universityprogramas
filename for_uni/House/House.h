#ifndef HOUSE_HOUSE_H
#define HOUSE_HOUSE_H

#include <iostream>

using  namespace std;
class House {
protected:
    int amount_of_floore;
    int amount_of_exit;
public:
    House();
    House(int amount_of_floore, int amount_of_exit);

    int getAmount_of_floore() const;

    virtual void House_stily() = 0;

};


#endif //HOUSE_HOUSE_H
