#ifndef HOUSE_RESENTIAL_H
#define HOUSE_RESENTIAL_H


#include "House.h"

class Resential: public House {
private:

    int amount_of_apertaments;
    int amount_of_balconys;
public:
    Resential();

    void House_stily() override;

    Resential(int amount_of_floore, int amount_of_exit, int amount_of_apertaments, int amount_of_balconys);
};


#endif //HOUSE_RESENTIAL_H
