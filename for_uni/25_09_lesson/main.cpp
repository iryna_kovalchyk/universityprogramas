#include <iostream>



class Shape{
protected:
    double  x0, y0;
public:
    Shape (double x, double y);
    virtual double area();
};

Shape::Shape(double x, double y) {
    x0 = x;
    y0 = y;
}

double Shape::area() {
    return  0;
}

class Circle : public Shape{
protected:
    double  radius;
public:
    Circle (double x, double y, double r);
    double area();
};

Circle::Circle(double x, double y, double r):Shape(x, y) {
    radius = r;
}

double Circle::area() {
    return 3.14159 * radius *radius;
}

class Cylindr : public Circle{
protected:
    double ht;
public:
    Cylindr(double x, double y, double r, double h);

    double area();
    double vol();
};
Cylindr ::Cylindr(double x, double y, double r, double h) : Circle(x,y,r) {
    ht = h;
}

double Cylindr::vol() {
    return Circle ::area() *ht;
}
double Cylindr ::area() {
    return 6.283*radius*ht;
}

class Rect: public Shape{
protected:
    double wd, ht;
public:
    Rect(double x, double y, double w, double h);
    double area();
};

Rect::Rect(double x, double y, double w, double h): Shape(x,y) {
    wd = w;
    ht = h;
}

double Rect::area() {
    return wd*ht;
}

class Box3d: public Rect{
protected:
    double depth;
public:
    Box3d(double x, double y, double w, double h, double d);
    double  area();
    double area2();
    double vol();
};

Box3d::Box3d(double x, double y, double w, double h, double d): Rect(x,y,w,h) {
    depth = d;
}
double Box3d::vol() {
    return Rect::area()*depth;
}

double Box3d::area() {
    return 2.0*(Rect::area() + Rect(0,0,ht,depth).area() + Rect(0,0,wd,depth).area());
}

double Box3d::area2() {
    return 2.0 * (Rect::area() + ht*depth + wd*depth);
}




using namespace std;

int main() {
    cout << "Shape \n";
    Shape a(0.0,0.0);
    cout << "area ="<< a.area()<<endl;

    cout<<"circle \n";
    Circle b(1.0,0.0,2.0);
    Circle * p = &b;
    cout<<"area"<<b.area()<<endl;
    cout<<"area" <<p->area()<<endl;

    Circle * p1 = new Circle(1.0,0.0,2.0);
    cout <<"area"<<p1->area()<<endl;

    Circle * p2 = new Circle(1.0,0.0,2.0);
    cout <<"area"<<p2->area()<<endl;

    cout <<"Cylindr\n";
    Cylindr c(0.0,0.0,2.0,5.0);
    p = &c;
    cout <<"area = "<<c.area()<< endl;
    cout<<"area = "<<p->area()<<endl;
    cout<<"volume ="<<c.vol()<< endl;

    Circle* p3 = &c;
    cout<< "area" <<p3->area()<<endl;
    b = c;
    cout<<"area = "<<b.area()<< endl;

    Shape* p4 = &c;
    cout<<"area "<<p4->area()<< endl;


    cout <<"Rect\n";
    Cylindr d(2.0,3.0,1.0,4.0);
    cout<<"area ="<<d.area()<<endl;

    cout<< "Box3d\n";
    Box3d f(0.0,0.0,1.0,2.0,3.0);
    cout<<"area "<<f.area()<<endl;
    cout <<"area ="<< f.area2()<<endl;
    cout << "volume ="<< f.vol()<<endl;

    return  0;
}