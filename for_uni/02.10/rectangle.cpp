#include <iostream>
#include <cmath>
using namespace std;

class point;
class rect;

class point
{

	int x,y;
  public:
	point():x(0),y(0){}
	point(int x1,int y1):x(x1),y(y1){}
	float distance(point& b)
	{ return sqrt((x-b.x)*(x-b.x)+(y-b.y)*(y-b.y));}
	friend istream& operator>>(istream& is,point& z);
	friend rect;
};

istream& operator>>(istream& is,point& z)
{
	is>>z.x>>z.y;
	return is;
}

class rect
{

	point a,b;
  public:
	rect(point aa,point bb)
	{
		a=aa; b=bb;
	}
	float area()
	{	point c(b.x,a.y);
		float w=a.distance(c);
		float d=b.distance(c);
		return w*d;
	}
	
};

void main()
{
	
	point h,l;
	cout<<"Input left point:";
	cin>>h;
	cout<<"Input right point:";
	cin>>l;
	
	rect r(h,l);
	cout<<"It's area equal "<<r.area()<<endl;
}

/*
Input left point:1 1
Input right point:4 6
It's area equal 15
*/