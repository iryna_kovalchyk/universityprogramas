#include <iostream>
#include <string>

using namespace std;

// ��������� ������� --------------------

class Published
{
  protected:
	  string name;
  public:
	  virtual void info() const=0;
};

// ����� --------------------------------

class Book:public Published
{
  protected: 
	  string autor;
  public:
	  Book():autor("none"){name="none";}
	  Book(string a_autor, string a_name);
	  Book(const Book&);
	  virtual void info() const;
	  friend istream& operator>>(istream&, Book&);
};

Book::Book(string a_autor, string a_name):autor(a_autor)
{
	name=a_name; // ����� ���� ����� ����� ��������� � ������ ������������
}

Book::Book(const Book& p)
{
	autor=p.autor;
	name=p.name;
}

void Book::info() const
{
	cout<<"Book : "<<autor<<", "<<name<<endl;
}

istream& operator>>(istream& is, Book& p)
{
	cout<<"Input BOOK data:\n";
	cout<<"  1. Name  = ";
	is>>p.name;
	cout<<"  2. Autor = ";
	is>>p.autor;
	return is;
}

// ��������� ������� -------------------

class Periodic:public Published
{
  protected: 
	  int period;
  public:
	  Periodic():period(0){name="none";}
	  Periodic(int a_period, string a_name);
	  Periodic(const Periodic&);
	  virtual void info() const;
	  friend istream& operator>>(istream&, Periodic&);
};

Periodic::Periodic(int a_period, string a_name):period(a_period)
{
	name=a_name; 
}

Periodic::Periodic(const Periodic& p)
{
	period=p.period;
	name=p.name;
}
void Periodic::info() const
{
	cout<<name<<", "<<period;
}

istream& operator>>(istream& is, Periodic& p)
{
	cout<<"Input PERIODIC data:\n";
	cout<<"  1. Name  = ";
	is>>p.name;
	cout<<"  2. Period = ";
	is>>p.period;
	return is;
}

// ��������� �������: ������ -----------

class Newspaper:public Periodic
{
  protected: 
	  string place;
  public:
	  Newspaper():Periodic(),place("none"){}
	  Newspaper(int a_period, string a_name, string a_place);
	  Newspaper(const Newspaper&);
	  virtual void info() const;
	  friend istream& operator>>(istream&, Newspaper&);
	
};

Newspaper::Newspaper(int a_period, string a_name, string a_place)
:Periodic(a_period,a_name)
{
	place=a_place; 
}

Newspaper::Newspaper(const Newspaper& p):Periodic(p)
{
	place=p.place;
}


void Newspaper::info() const
{
	cout<<"Newspaper : ";
	Periodic::info();
	cout<<", "<<place<<endl;
}

istream& operator>>(istream& is, Newspaper& p)
{
	cout<<"Input NEWSPAPER data:\n";
	cout<<"  1. Name   = ";
	is>>p.name;
	cout<<"  2. Period = ";
	is>>p.period;
	cout<<"  3. Place  = ";
	is>>p.place;
	return is;
}

// ��������� �������: ������� ----------

class Magazine:public Periodic
{
  protected: 
	  int pages;
  public:
	  Magazine():Periodic(),pages(0){}
	  Magazine(int a_period, string a_name, int a_pages);
	  Magazine(const Magazine&);
	  virtual void info() const;
	  friend istream& operator>>(istream&, Magazine&);
	
};

Magazine::Magazine(int a_period, string a_name, int a_pages)
:Periodic(a_period,a_name)
{
	pages=a_pages; 
}

Magazine::Magazine(const Magazine& p):Periodic(p)
{
	pages=p.pages;
}

void Magazine::info() const
{
	cout<<"Magazine : ";
	Periodic::info();
	cout<<", "<<pages<<endl;
}

istream& operator>>(istream& is, Magazine& p)
{
	cout<<"Input MAGAZINE data:\n";
	cout<<"  1. Name   = ";
	is>>p.name;
	cout<<"  2. Period = ";
	is>>p.period;
	cout<<"  3. Pages  = ";
	is>>p.pages;
	return is;
}


void main()
{
	Published** vector;
	
	int n;
	cout<<"Input number of published: ";
	cin>>n;
	
	vector=new Published* [n];

	for(int i=0;i<n;i++)
	{
		cout<<"Choose: 1 - Book, 2 - Nespaper, 3 - Magazine?\n";
		int c;
		cin>>c;
		switch (c)
		{
		  case 1:
			{
				Book t;
				cin>>t;
				vector[i]=new Book(t); //=&t �� �����, �� t - �������� �����
				break;
			}
		  case 2:
			{
				Newspaper t;
				cin>>t;
				vector[i]=new Newspaper(t);
				break;
			}
		  case 3:
			{
				Magazine t;
				cin>>t;
				vector[i]=new Magazine(t);
				break;
			}
		  default:
			cout<<"ERROR!"<<endl;
		}
	}
	
	cout<<"\n\n*****INFORMATION!*****\n";
	for (i=0;i<n;i++) vector[i]->info();
	cout<<endl;
}

/*
Input number of published: 5
Choose: 1 - Book, 2 - Nespaper, 3 - Magazine?
3
Input MAGAZINE data:
  1. Name   = Lisa
  2. Period = 1
  3. Pages  = 30
Choose: 1 - Book, 2 - Nespaper, 3 - Magazine?
1
Input BOOK data:
  1. Name  = DreamCatcher
  2. Autor = King
Choose: 1 - Book, 2 - Nespaper, 3 - Magazine?
2
Input NEWSPAPER data:
  1. Name   = Postyp
  2. Period = 2
  3. Place  = Lviv
Choose: 1 - Book, 2 - Nespaper, 3 - Magazine?
1
Input BOOK data:
  1. Name  = C+=
  2. Autor = Straustrup
Choose: 1 - Book, 2 - Nespaper, 3 - Magazine?
2
Input NEWSPAPER data:
  1. Name   = Fakty
  2. Period = 3
  3. Place  = Kyiv


*****INFORMATION!*****
Magazine : Lisa, 1, 30
Book : King, DreamCatcher
Newspaper : Postyp, 2, Lviv
Book : Straustrup, C+=
Newspaper : Fakty, 3, Kyiv

*/