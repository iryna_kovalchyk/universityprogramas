#include <iostream>
using namespace std;

class figure
{
  protected:
	  float x,y;
  public:
	  figure():x(0),y(0){}
	  figure(float xx,float yy):x(xx),y(yy){}
	  virtual float area() const=0;
	  virtual ~figure(){}
};

class circle:public figure
{
	  float rad;
  public:
	  circle():figure(),rad(0){}
	  circle(float xx,float yy,float r):figure(xx,yy),rad(r){}
	  virtual float area() const;
	  void print();
	  virtual ~circle(){}
};

class rectangle:public figure
{
	  float a,b;
  public:
	  rectangle():figure(),a(0),b(0){}
	  rectangle(float xx,float yy,float aa,float bb):figure(xx,yy),a(aa),b(bb){}
	  virtual float area() const;
	  void print();
	  virtual ~rectangle(){}
};

class volfigure
{
	  figure* base;
      float height;
  public:
	  volfigure():base(0),height(0){}
	  volfigure(figure* b, double h){ base=b; height=h;}
	  double volume();
	  ~volfigure(){}
};

float circle::area() const
{
	return 3.14*rad*rad;
}

void circle::print()
{
	cout<<"x="<<x<<", y="<<y<<", r="<<rad<<endl;
}

float rectangle::area() const
{
	return a*b;
}

void rectangle::print()
{
	cout<<"x="<<x<<", y="<<y<<", a="<<a<<", b="<<b<<endl;
}

double volfigure::volume()
{
	return base->area()*height;
}

void main()
{
	cout<<"circle\n";
	circle a(1,2,2);
	figure* pa=&a;
	a.print();
	cout<<"area: ";
	cout<<pa->area()<<endl;
	cout<<a.area()<<endl;
	volfigure va(&a,3);
	cout<<"volume = "<<va.volume()<<endl;
		
	cout<<"\nrectangle\n";
	rectangle b(1,2,2,5);
	figure* pb=&b;
	b.print();
	cout<<"area: ";
	cout<<pb->area()<<endl;
	cout<<b.area()<<endl;
	volfigure vb(pb,3);
	cout<<"volume = "<<vb.volume()<<endl;

	cout<<"\nrectangle\n";
	figure* pc=new rectangle(1,2,2,5);
	cout<<"area: ";
	cout<<pc->area()<<endl;
	volfigure vc(pc,3);
	cout<<"volume = "<<vc.volume()<<endl;

}

/*
circle
x=1, y=2, r=2
area: 12.56
12.56
volume = 37.68

rectangle
x=1, y=2, a=2, b=5
area: 10
10
volume = 30

rectangle
area: 10
volume = 30
*/