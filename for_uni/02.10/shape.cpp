#include <iostream>
using namespace std;

class shape
{
	protected:
		double x0, y0;
	public:
		shape(double x, double y);
		virtual double area();
};

shape::shape(double x, double y)
{
	x0=x;	y0=y;
}

double shape::area()
{ 
	return 0;
}

class circle:public shape
{
	protected:
		double radius;
	public:
        circle(double x,double y,double r);
        double area();     
};

circle::circle(double x, double y, double r):shape(x,y)
{
	radius=r;
}

double circle::area()
{
	return 3.14159*radius*radius;
}

class cylinder:public circle
{
	protected:
		double ht;
	public:
        cylinder(double x, double y, double r, double h);
        double area();
        double vol();
};

cylinder::cylinder(double x,double y,double r,double h):circle(x,y,r)
{
	ht=h;
}

double cylinder::vol()
{ 
	return circle::area()*ht; 
}

double cylinder::area()
{
	return 6.283*radius*ht;
}

class rect:public shape
{
	protected:
		double wd, ht;
	public:
        rect(double x, double y, double w, double h);
        double area();
};

rect::rect(double x, double y, double w, double h):shape(x,y)
{
	wd = w;
	ht = h;
}

double rect::area()
{
	return wd*ht;
}

class box3d:public rect
{
	protected:
		double depth;
	public:
        box3d(double x, double y, double w, double h, double d);
        double area();
		double area2();
        double vol();
};

box3d::box3d(double x,double y,double w,double h,double d):rect(x,y,w,h)
{
	depth = d;
}

double box3d::vol()
{
	return rect::area()*depth;
}

double box3d::area()
{
	return	2.0*(rect::area()+rect(0,0,ht,depth).area()
			+rect(0,0,wd,depth).area());
}

double box3d::area2()
{
	return	2.0*(rect::area()+ht*depth+wd*depth);
}

void main()
{
	// ���� ������
	cout<<"shape\n";
	shape a(0,0);
	cout<<"area="<<a.area()<<endl;
	
	// ���� ����
	cout<<"circle\n";
	circle b(1,0,2);
	circle* p=&b;
	cout<<"area="<<b.area()<<endl;
	cout<<"area="<<p->area()<<endl;
	
	circle* p1=new circle(1,0,2);
	cout<<"area="<<p1->area()<<endl;
	
	circle* p2;
	p2=new circle(1,0,2);
	cout<<"area="<<p2->area()<<endl;
	
	// ���� ������
	cout<<"cylinder\n";
	cylinder c(0,0,2,5);
	p=&c;
	cout<<"area="<<c.area()<<endl;
	cout<<"area="<<p->area()<<endl;
	cout<<"volume="<<c.vol()<<endl;

	circle* p3=&c;
	cout<<"area="<<p3->area()<<endl;

	b=c;
	cout<<"area="<<b.area()<<endl;

	shape* p4=&c;
	cout<<"area="<<p4->area()<<endl;

	// ���� �����������
	cout<<"rect\n";
	cylinder d(2,3,1,4);
	cout<<"area="<<d.area()<<endl;
	
	// ���� ������������
	cout<<"box3d\n";
	box3d f(0,0,1,2,3);
	cout<<"area="<<f.area()<<endl;
	cout<<"area="<<f.area2()<<endl;
	cout<<"volume="<<f.vol()<<endl;
}

/*
shape
area=0
circle
area=12.5664
area=12.5664
area=12.5664
area=12.5664
cylinder
area=62.83
area=62.83
volume=62.8318
area=62.83
area=12.5664
area=62.83
rect
area=25.132
box3d
area=22
area=22
volume=6
*/