#include <iostream>
#include <fstream>
#include <cstring>

using namespace std;

template <typename type>
class Stack {
private:
    type *array;
    int size;
    int index;
public:
    Stack(){
        size = 0;
        index = 0;
        array = new type[size];
    }
    Stack(int size){
        index = 0;
        this->size = size;
        array = new type[size];
    }
    ~Stack(){
        delete []array;
    }
    void pushToEndIfConst(type val) { // use when size of massive is const
        array[index] = val;
        index++;
    }
    void pushToEndIfUnknown(type val){ //use when need dynamic massive
        type *copyArray = array;
        size++;
        array = new type[size];
        for (int i = 0; i < size; i++) {
            array[i] = copyArray[i];
        }
        array[index] = val;
        index++;
        delete []copyArray;
    }
    void pushToStartIfConst(type val) { // use when size of massive is const
        for (int i = index; i > 0; i--) {
            array[i] =  array[i - 1];
        }
        array[0] = val;
        index++;
    }
    void pushToStartIfUnknown(type val){ //use when need dynamic massive
        type *copyArray = array;
        size++;
        array = new type[size];
        for (int i = 0; i < size; i++) {
            array[i + 1] = copyArray[i];
        }
        array[0] = val;
        index++;
        delete []copyArray;
    }
    void popFromEndIfUnknown(){
        type *copyArray = array;
        size--;
        array = new type[size];
        for (int i = 0; i < size; i++){
            array[i] = copyArray[i];
        }
        index--;
        delete []copyArray;
    }
    void popFromStartIfUnknown(){
        type *copyArray = array;
        size--;
        array = new type[size];
        for (int i = 0; i < size; i++){
            array[i] = copyArray[i + 1];
        }
        index--;
        delete []copyArray;
    }
    void popFromEndIfConst(){
        array[index] = NULL;
        index--;
    }
    void popFromStartIfConst(){
        for (int i = 1; i < index; i++) {
            array[i - 1] = array[i];
        }
        array[index] = NULL;
        index--;
    }
    void view(){
        cout     << "Result is: ";
        for (int i = 0; i < size; i++) {
            cout << array[i];
        }
    }
};

int main() {
    string textToAdd;
    string finalText;
    string text;
    ifstream file("C://Users//kov_i//CLionProjects//ira//file");
    if (!file.is_open()){
        cout << "Wrong path!";
        return 0;
    };
    while (!file.eof()) {
        file >> textToAdd;
        textToAdd += '\n';
        finalText = textToAdd;
        finalText += text;
        text = finalText;
    }
    file.close();
    cout     << text
             << endl
             << endl;
    Stack<char> reverse(text.length() + 1);
    for (int i = text.length(); i > -1; i--) {
        reverse.pushToEndIfConst(text[i]);
    }
    reverse.view();
    return 0;

}
