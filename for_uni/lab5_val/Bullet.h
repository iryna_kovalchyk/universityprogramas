#ifndef LAB5_VAL_BULLET_H
#define LAB5_VAL_BULLET_H

#include "Abst.h"
#include <cmath>


class Bullet: public Abst {
private:
    double r;
public:
    Bullet();

    Bullet(double r);

    double value() override;
};


#endif //LAB5_VAL_BULLET_H
