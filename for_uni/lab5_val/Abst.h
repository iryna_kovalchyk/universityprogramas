
#ifndef LAB5_VAL_ABST_H
#define LAB5_VAL_ABST_H


class Abst {
protected:
    virtual double value() = 0;
};


#endif //LAB5_VAL_ABST_H
