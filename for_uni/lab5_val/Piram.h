#ifndef LAB5_VAL_PIRAM_H
#define LAB5_VAL_PIRAM_H

#include "Abst.h"

class Piram: public Abst {
private:
    double x;
    double  y;
    double h;
public:
    Piram();

    Piram(double x, double y, double h);

    double value() override;
};


#endif //LAB5_VAL_PIRAM_H
