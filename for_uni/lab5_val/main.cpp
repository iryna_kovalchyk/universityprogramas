#include <iostream>
#include "Abst.h"
#include "Piram.h"
#include "Bullet.h"

using namespace std;

int main() {
    Piram piram(1.0, 2.0, 4.0);
    Bullet bullet(1.0);
    cout << "value of piram: ";
    cout << piram.value()<< endl;
    cout << "value of bullet: ";
    cout << bullet.value()<< endl;
    return 0;
}